package com.jloaiza.monitor.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by jloaiza
 * Todos los derechos reservados.
 */
@Controller
@RequestMapping("/")
public class HomeController {

    @GetMapping("/")
    public ModelAndView home ()
    {
        return new ModelAndView("index");
    }

    @GetMapping("/profile")
    public ModelAndView interfaces()
    {
        return new ModelAndView("profile");
    }
}
