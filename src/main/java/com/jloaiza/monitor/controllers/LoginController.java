package com.jloaiza.monitor.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * Created by JLOAIZA
 * Todos los derechos reservados.
 */
@Controller
@RequestMapping ("/")
public class LoginController {
@GetMapping("/login")
public ModelAndView Login ()
{
    return new ModelAndView("login");
}


}
